require "test_helper"

describe "UUID" do
  describe "::generate" do
    it "returns a valid uuid" do
      UUID.generate.must_match %r{[a-z0-9-]}
    end
  end
end
