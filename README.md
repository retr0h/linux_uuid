# linux_uuid

A non-portable, fast, simple, atomic UUID generator.

## Usage

### Bundler

    gem "linux_uuid"

### Example

    UUID.generate

## Testing

    $ bundle exec rake

## Dependencies

Uses uuidgen(1) for compatability with Snow Leopard developers.

* linux:/proc/sys/kernel/random/uuid
