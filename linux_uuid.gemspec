# -*- encoding: utf-8 -*-
$:.push File.expand_path("../lib", __FILE__)
require "linux_uuid/version"

Gem::Specification.new do |s|
  s.name        = "linux_uuid"
  s.version     = LinuxUuid::VERSION
  s.platform    = Gem::Platform::RUBY
  s.authors     = ["John Dewey"]
  s.email       = ["john@dewey.ws"]
  s.homepage    = %q{http://github.com/retr0h/linux_uuid}
  s.summary     = %q{A non-portable, fast, simple, atomic UUID generator.}

  s.rubyforge_project = "linux_uuid"

  s.files         = `git ls-files`.split("\n")
  s.test_files    = `git ls-files -- {test,spec,features}/*`.split("\n")
  s.executables   = `git ls-files -- bin/*`.split("\n").map{ |f| File.basename(f) }
  s.require_paths = ["lib"]

  s.add_dependency "io-extra", "~> 1.2.5"

  s.add_development_dependency "rake"
  s.add_development_dependency "webmock"
end
