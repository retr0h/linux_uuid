require "io/extra"

class UUID
  FP = File.open "/proc/sys/kernel/random/uuid", "rb"
  FD = FP.fileno

  ##
  # Atomic read of Linux's UUID implementation.

  def self.generate
    IO.pread(FD, 36, 0)
  end

rescue Errno::ENOENT
  define_singleton_method :generate do
    %x{uuidgen}
  end
end
